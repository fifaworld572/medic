import { createApp } from 'vue'
import {createRouter, createWebHistory} from 'vue-router';
import App from './App.vue'
import routerLink from '@/router.js'


const routes = routerLink;

const router = createRouter({
  routes,
  history: createWebHistory(process.env.BASE_URL),
});


const app = createApp(App)

app
  .use(router)
  .mount('#app')
