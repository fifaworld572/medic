import CreateProtocol from "@/pages/CreateProtocol";
import EditSample from "@/pages/EditSimple";

export default [
    {
      path: "/",
      component: CreateProtocol,
      title: 'Создание протокола'
    },
    {
      path: "/editSample/:id",
      component: EditSample,
      title: 'Редактирование шаблона'
    },
]
